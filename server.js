const express = require('express');
const path = require('path');
const port = process.env.PORT || 8080;

var app = express();

console.log(__dirname);
app.use(express.static(__dirname + '/dist'));

app.listen(port);
console.log('server started');
